'use strict';

/**
 * @ngdoc function
 * @name crossoverTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the crossoverTestApp
 */
angular.module('crossoverTestApp')
  .controller('MainCtrl', function ($scope) {
    $scope.items = [
      {
        build: 'Tenrox-R1_1235',
        state: 'Pending',
        collapsed: true
      },
      {
        changelist: '432462',
        owner: 'jtuck',
        timeStarted: new Date("April 18, 2014 12:12:00"),
        state: 'Running',
        metrics: '70',
        details: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.',
        collapsed: true
      },

    ];

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
