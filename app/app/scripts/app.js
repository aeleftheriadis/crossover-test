'use strict';

/**
 * @ngdoc overview
 * @name crossoverTestApp
 * @description
 * # crossoverTestApp
 *
 * Main module of the application.
 */
angular
  .module('crossoverTestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
