'use strict';

/**
 * @ngdoc directive
 * @name crossoverTestApp.directive:accordion
 * @description
 * # accordion
 */
angular.module('crossoverTestApp')
  .directive('accordion', function () {
    return {
      scope:{
        ngModel: '='
      },
      restrict: 'A',
      templateUrl: 'views/directives/accordion.html'
    };
  });
