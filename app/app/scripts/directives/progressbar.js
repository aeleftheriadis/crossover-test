'use strict';

/**
 * @ngdoc directive
 * @name crossoverTestApp.directive:progressbar
 * @description
 * # progressbar
 */
angular.module('crossoverTestApp')
  .directive('progressbar', function () {
    return {
      scope: {
        progress: '='
      },
      templateUrl: 'views/directives/progressbar.html',
      restrict: 'E'
    };
  });

